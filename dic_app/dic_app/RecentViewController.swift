//
//  ViewController.swift
//  hw2_core_data-Clark
//
//  Created by Allen Clark on 2018/10/21.
//  Copyright © 2018 Allen Clark. All rights reserved.
//

/*
 // TODO
 [ ] Store into coredata
 [ ] Programly load Dic VC
 */

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

class RecentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // ----- Outlets
    @IBOutlet weak var recentSearch: UITextField!
    @IBOutlet weak var recentTable: UITableView!
    
    // ----- Vars 
    var jishoJson : JSON!
    var fetchedEntries : [FetchedData] = []  // This will hold the entries. Will be init later. [String : [String]]
    var sentSearchSelection : String = ""
    var queries : [NSManagedObject] = [] // will hold the user entered queries
    // -----
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recentTable.tableFooterView = UIView(frame: CGRect.zero) // removes extra cells
        
        self.jishoJson = nil
        
        recentTable.rowHeight = UITableView.automaticDimension
        recentTable.estimatedRowHeight = 140
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        recentTable.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard (UIApplication.shared.delegate as? AppDelegate) != nil else { return }
        
        // - this will check if any data has been passed from an external tableviewcontroller
        if(!sentSearchSelection.isEmpty) {
            self.recentSearch.text = sentSearchSelection
            self.fetchTranslationJisho(query: sentSearchSelection)
            sentSearchSelection = ""
        }
    }
    // ----- 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning() // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedEntries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listEntryCell") as! ListEntryCell
        let singleEntry = fetchedEntries[indexPath.row]
        
        cell.setCellData(recievedWord: singleEntry.word, recievedReading: singleEntry.read, recievedTranslation: singleEntry.trans)
        
        return cell
    }
    
    // ----- Actions
    @IBAction func invokeQuery(_ sender: Any) {
        print(">>>>> Adding query: \(String(describing: recentSearch?.text))")
        
        let query = recentSearch.text!
        
        if(query.isEmpty || query == "New search...") { return } // if it is empty or just the first base string
        
        fetchTranslationJisho(query : query)

        print(">>>>> Post Append: \(queries)")
        
        recentSearch.resignFirstResponder() // disimis keyboard
        
        self.recentTable.reloadData() // refresh the table
    }
    
    // write the word recived from jisho to core data
    func writeData(query : FetchedData) {
        print(">>>>> Writing Data: \(query)")
        
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDel.persistentContainer.viewContext // this is the context of the core data
        
        let queryEntity = NSEntityDescription.entity(forEntityName: "Query", in: managedContext)!
        let queryObj = NSManagedObject(entity: queryEntity, insertInto: managedContext)
        
        queryObj.setValue(query.word, forKeyPath: "text")
        queryObj.setValue(query.read, forKeyPath: "read")
        queryObj.setValue(query.trans, forKeyPath: "trans")
        
        do {
            try managedContext.save()
            
            self.queries.append(queryObj)
            
            print(">>>>> Data Written \(queryObj)")
        } catch let err as NSError {
            print("<<<<< Failed to save to Core Data \(err)")
        }
    }
    // ----- 
    
    // ----- Fetch and Handle JSOn
    // This will fetch JSON from Jisho.org
    func fetchTranslationJisho(query : String) {
        // clear out table before getting new data
        if(!fetchedEntries.isEmpty) {
            fetchedEntries.removeAll()
        }
        
        
        let jishoBase = "https://jisho.org/api/v1/search/words" // Base url used to search jisho.org

        // Need to use percent encoding
        // Basing off of: https://stackoverflow.com/questions/38359709/how-to-use-non-english-string-in-nsurl
        var urlComp = URLComponents(string : jishoBase)!
        
        urlComp.queryItems = [
            URLQueryItem(name : "keyword", value : query)
        ]
        
        guard let jishoEncodeUrl = urlComp.url else { return }
        
        // Sending the request using Alamofire, baby
        Alamofire.request(jishoEncodeUrl).responseJSON{ (respData) in
            switch respData.result {
                case .success(let val):
                    self.jishoJson = JSON(val)

                    // ----- debugging -----
                    print(self.jishoJson["data"][0]["japanese"][0]["word"])
                    print(self.jishoJson["data"][0]["japanese"][0]["reading"])
                    print(self.jishoJson["data"][0]["senses"][0]["english_definitions"][0])
                    print("-----")
                    // -----
                    
                    
                    for(_, sub) in self.jishoJson["data"] {
                        // - handle the default word values for when there's no kanji
                        var word : String = sub["japanese"][0]["word"].rawString()!
                        if(word == "null") {
                            word = query // self.jishoJson["data"][0]["japanese"][0]["word"].rawString()!
                        }
                        
                        guard let searchedReading : String = sub["japanese"][0]["reading"].rawString() else {
                            return
                        }
                        
                        // - parse english sub json
                        let transJson : JSON = sub["senses"][0]["english_definitions"] // don't need to unwrap b/c unwrap later
                        
                        var searchedTrans : String = ""
                        
                        for(_, subTrans) in transJson {
                            searchedTrans += "\(subTrans), "
                        }
                    
                        // - remove trailing comma and space
                        searchedTrans.removeLast()
                        searchedTrans.removeLast()
                        
                        self.gatherQueries(searchedWord : word, searchedReading : searchedReading, searchedTrans : searchedTrans)
                    }
                case .failure(let err):
                    print(err)
            }
        }
    }
    
    func gatherQueries(searchedWord : String, searchedReading : String, searchedTrans : String){
        let data : FetchedData = FetchedData(word : searchedWord, reading : searchedReading, translation : searchedTrans)
        
        // TODO need to check to see if it is already in the list https://medium.com/@iostechset/contains-function-in-swift-5e0276638eb
    
        self.fetchedEntries.append(data)
        
        recentTable.reloadData()
    }
    // -----
    
    // ----- Table Gestures https://www.youtube.com/watch?v=RSxfGGdA8QE
    // unstar
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listEntryCell") as! ListEntryCell // Cell has all the formatting
        
        // only show unstar if stared
        if(fetchedEntries[indexPath.row].tag == "STAR") {
            let unstar = UIContextualAction(style: .destructive, title: "Unstar") { (action, view, nil) in
                // this should check that it is stared first
                self.fetchedEntries[indexPath.row].removeTag()
                print(">>>>> unstared")
            }
           
            // image / bg
            unstar.backgroundColor = cell.unstarBG
            unstar.image = cell.unstarImage
            
            let action = UISwipeActionsConfiguration(actions : [unstar])
            action.performsFirstActionWithFullSwipe = false // make it so that it doesn't do full swipe to unstar cauase that is annoying
            
            self.fetchedEntries[indexPath.row].removeTag()
            
            return action
        } else if (fetchedEntries[indexPath.row].tag == "COMMON") {
            let uncommon = UIContextualAction(style: .destructive, title: "Uncommon") { (action, view, nil) in
                // this should check that it is stared first
                self.fetchedEntries[indexPath.row].removeTag()
                print(">>>>> uncommoned")
            }
            
            // image / bg
            uncommon.backgroundColor = cell.uncommonBG
            uncommon.image = cell.uncommonImage

            let action = UISwipeActionsConfiguration(actions : [uncommon])
            
            return action
        }
        
        return nil // if no tags then...
    }
    
    // star and common
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listEntryCell") as! ListEntryCell // Cell has all the formatting
        
        let star = UIContextualAction(style: .normal, title: "Star") { (action, view, nil) in
            print(">>>>> stared")

            self.fetchedEntries[indexPath.row].setTag(tag: "STAR")
            
            self.writeData(query: self.fetchedEntries[indexPath.row]) // writing the data that was stared to core data
        }
        
        // image / bg
        star.backgroundColor = cell.starBG
        star.image = cell.starImage
        
        let common = UIContextualAction(style: .normal, title: "Common") { (action, view, nil) in
            print(">>>>> commoned")

            self.fetchedEntries[indexPath.row].setTag(tag: "COMMON")
        }
        
        // image / bg
        common.backgroundColor = cell.commonBG
        common.image = cell.commonImage
        
        return UISwipeActionsConfiguration(actions : [star]) // TODO "common" will be added in a later patch
    }
    // -----
}

/*
 
 Will be refactring the way that FettchedData is hanlded.
 it will mangage all of its junk like parsing and stuff
 
 Will also only store things in a core data only if it is staredl
*/
