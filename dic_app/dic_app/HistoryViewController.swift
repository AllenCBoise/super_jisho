//
//  SecondaryViewController.swift
//  hw2_core_data-Clark
//
//  Created by Allen Clark on 2018/10/22.
//  Copyright © 2018 Allen Clark. All rights reserved.
//

import UIKit
import CoreData

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // ----- Outlets
    @IBOutlet weak var historyTable: UITableView!
    @IBOutlet weak var historySearch: UITextField!
    
    // ----- Vars
    var userSearchResult : [NSManagedObject] = [] // hold the user searches
    // -----
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userSearchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listEntryCell", for: indexPath) as! ListEntryCell
        
        if(userSearchResult.isEmpty) { return cell }
        let singleSearch = userSearchResult[indexPath.row]
    

        // setting the fields of the cell to that of the object retreived from core data
        if(singleSearch.value(forKey: "text") != nil) {
            let wordData = singleSearch.value(forKeyPath: "text") as! String
            let readData = singleSearch.value(forKeyPath: "read") as! String
            let transData = singleSearch.value(forKeyPath: "trans") as! String
            
            cell.setCellData(recievedWord: wordData, recievedReading: readData, recievedTranslation: transData)
            return cell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    // -----
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        historyTable.tableFooterView = UIView(frame: CGRect.zero) // removes extra cells
        
        historyTable.rowHeight = UITableView.automaticDimension
        historyTable.estimatedRowHeight = 140
        
        historyTable.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        historyTable.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDel.persistentContainer.viewContext
        let queryFetch = NSFetchRequest<NSManagedObject>(entityName: "Query")

        do {
            self.userSearchResult = try managedContext.fetch(queryFetch)
        } catch let err as NSError {
            print("<<<<< Could not fetch core data \(err)")
        }
    }
    
    // ----- Swipe Gestures
    // Unstar
//    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//        let unstar = UIContextualAction(style: .destructive, title: "Unstar") { (action, view, nil) in
//            print(">>>>> unstared")
//            self.deleteRowSelection(indexPath: indexPath)
//        }
//
//        let action = UISwipeActionsConfiguration(actions : [unstar])
//        action.performsFirstActionWithFullSwipe = false // make it so that it doesn't do full swipe to unstar cauase that is annoying
//
//        return action
//    }
    
    // for somereason it was not working with the method above which is what i used for recent table
    // it would not update the index and would get null pointers
    // followed https://www.youtube.com/watch?v=MC4mDQ7UqEE instead and it is a lot better
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete) {
            historyTable.beginUpdates()
            self.deleteRowSelection(indexPath: indexPath)
            self.userSearchResult.remove(at: indexPath.row)
            historyTable.deleteRows(at: [indexPath], with: .fade)
            historyTable.endUpdates()
        }
    }
    
    // -----
    
    // ----- Actions -----
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.deleteRowSelection(indexPath: indexPath)
        historyTable.reloadData()
    }
    
    // Sends a search to RecentViewController
    @IBAction func invokeQuery(_ sender: Any) {
        // - will send the data do the recentViewController to have searched
        guard let search : String = self.historySearch.text else {
            return
        }
        guard let recentTab : RecentViewController = self.tabBarController?.viewControllers?[1] as? RecentViewController else {
            return
        }
        recentTab.sentSearchSelection = search
        self.tabBarController?.selectedIndex = 1
        
        self.historySearch.resignFirstResponder()
        
        print(">>>>> Searching from history view: \(search)")
    }
    
    func deleteRowSelection(indexPath: IndexPath) {
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDel.persistentContainer.viewContext
        
        let cellToDelete = self.userSearchResult[indexPath.row] // this is NSMan obj that we are going to delete
        
        print("<<<<< Attempting to delete \(cellToDelete)")
        managedContext.delete(cellToDelete)
        
        do {
            try managedContext.save()
        } catch let err as NSError {
            print("<<<<< Failed to save to Core Data \(err)")
        }

        historyTable.reloadData()
    }
    // -----
}
