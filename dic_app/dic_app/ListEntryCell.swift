//
//  ListEntryCell.swift
//  dic_app
//
//  Created by Allen Clark on 2018/12/04.
//  Copyright © 2018 Allen Clark. All rights reserved.
//

import UIKit

class ListEntryCell : UITableViewCell {
    // ----- Outlets
    @IBOutlet weak var recievedTranslation: UITextView!
    @IBOutlet weak var recievedReading: UITextView!
    @IBOutlet weak var recievedWord: UILabel!
    // -----
    
    // ----- Swipping Contexts
    let unstarImage = #imageLiteral(resourceName: "Unstar")
    let unstarBG = #colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1)
    let uncommonImage = #imageLiteral(resourceName: "Uncommon")
    let uncommonBG = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)
    let starBG = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1) // color literal is just "color"
    let starImage = #imageLiteral(resourceName: "Star") // image is just literal "image"
    let commonBG = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
    let commonImage = #imageLiteral(resourceName: "Common")
    // -----
    
    let bg = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    
    // Set the data that will appear in the table cell
    func setCellData(recievedWord : String, recievedReading : String, recievedTranslation : String) {
        self.recievedWord.text = recievedWord
        self.recievedReading.text = recievedReading
        self.recievedTranslation.text = recievedTranslation
    }
}
