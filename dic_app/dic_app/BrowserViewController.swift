//
//  BrowserViewController.swift
//  hw1_tabView_clark
//
//  Created by Allen Clark on 2018/09/22.
//  Copyright © 2018 Allen Clark. All rights reserved.
//

/*
 This will be used to collect and browse webpages
 Words selected on this webpage can be translated
 using JSHIO.org eventually.
 
 Used the following as guidance:
 https://www.youtube.com/watch?v=xQmZSKxOYvs&t=0s&list=PLP8n25bNfkdc4Vopq3lMi_iVsMdqXYGVj&index=10
 */

import UIKit
import WebKit

class BrowserViewController: UIViewController, UITextFieldDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var addressBar: UITextField!
    
    // ----- Vars
    var userAddress : String = "https://www3.nhk.or.jp/news/easy/"
    var userSelection : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ----- These ensure that this VC is can communicate with the attributes (callbacks and controls...)
        addressBar.delegate = self // connects the address bar to this VC -> makes it so that input from
        // from the bar goes to this VC
        webView.navigationDelegate = self
        // -----
        
        selectionMenu() // add the search menu item
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // TODO i should throw in a grep so that you can throw anything into the addressbar
        if(userAddress == "") {
            userAddress = "https://www3.nhk.or.jp/news/easy/"
        }
        
        let url : URL = URL(string: userAddress)!
        let urlReq : URLRequest = URLRequest(url : url)
        
        webView.load(urlReq)
        
        addressBar.text = userAddress
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userAddress = addressBar.text!
        
        // force to only go to Japanese websites
        if(!userAddress.contains(".jp")) {
            userAddress = userAddress.replacingOccurrences(of: ".com", with: ".co.jp")
        }
        
        if(!userAddress.starts(with: "https://")) {
            userAddress = "https://" + userAddress
        }
        
        let url : URL = URL(string : userAddress)!
        let urlReq : URLRequest = URLRequest(url : url)
        
        webView.load(urlReq)
        
        addressBar.resignFirstResponder() // dismisses the keyboard
        
        return true
    }
    
    // ----- Text Selection Menue -----
    // Will add a search item to the selection menu in the browser
    func selectionMenu() {
        let menuSearch = UIMenuItem(title: "Dictionary Search", action: #selector(sendToSearch))
        UIMenuController.shared.menuItems = [menuSearch]
    }
    
    @objc func sendToSearch() {
        // need to get the selected text so have to use JavaScript
        webView.evaluateJavaScript("window.getSelection().toString()", completionHandler: { (res, err) in
            if(res != nil) {
                self.userSelection = res as! String // it's string b/c javascript is the best language in the world 😊
            
                // - will send the data do the recentViewController to have searched
                guard let recentTab : RecentViewController = self.tabBarController?.viewControllers?[1] as? RecentViewController else {
                    return
                }
                recentTab.sentSearchSelection = self.userSelection
                self.tabBarController?.selectedIndex = 1
                
                print(">>>>> Searching from browser view: \(self.userSelection)")
            } else {
                print(">>>>> ERROR: \(String(describing: err))")
            }
        })
    }
    // -----
    
    // ----- Navigation buttons -----
    @IBAction func backTapped(_ sender: Any) {
        if ( webView.canGoBack ) {
            webView.goBack()
        }
    }
    
    @IBAction func forwardTapped(_ sender: Any) {
        if ( webView.canGoForward ) {
            webView.goForward()
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        backButton.isEnabled = webView.canGoBack
        forwardButton.isEnabled = webView.canGoForward
        
        addressBar.text = webView.url?.absoluteString
    }
    // -----
}
