//
//  FetchedData.swift
//  dic_app
//
//  Created by Allen Clark on 2018/12/04.
//  Copyright © 2018 Allen Clark. All rights reserved.
//

import Foundation

class FetchedData {
    // ----- JSON data -----
    var word : String
    var read : String
    var trans : String
    // -----
    
    var tag : String // valid by convention only: STAR, COMMON
    
//    enum Tag { // want to use these but idk how
//        case star, common
//    }
    
    init(word : String, reading : String, translation : String) {
        self.word = word
        self.read = reading
        self.trans = translation
        self.tag = ""
    }
    
    // ----- Tagging
    func setTag(tag : String) {
        self.tag = tag
    }
    
    // Removes tag
    // TODO this should be using ENUMs b/c of multiple tags
    func removeTag() {
        self.tag = ""
    }
    // -----
}
